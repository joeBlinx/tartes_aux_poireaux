use gl::types::*;
use gl;
use std::path::Path;
use sdl2::image::LoadSurface;
use sdl2::surface::Surface;
use std::convert::TryInto;

pub struct Texture{
    id: GLuint,
}

impl Texture{
    pub fn from_file(path_to_file:&Path) -> Texture{
        let sdl2_surface = Surface::from_file(path_to_file).unwrap();
        let id:GLuint = 0;
        unsafe{
            gl::GenTextures(1, [id].as_mut_ptr());
            gl::BindTextures(0, 1, [id].as_ptr());
            gl::TexImage2D(
                gl::TEXTURE_2D,
                0,
                gl::RGBA.try_into().unwrap(),
                sdl2_surface.width().try_into().unwrap(),
                sdl2_surface.height().try_into().unwrap(),
                0,
                gl::RGBA.try_into().unwrap(),
                gl::UNSIGNED_BYTE,
                (*sdl2_surface.raw()).pixels
            );
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST.try_into().unwrap());

            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST.try_into().unwrap());
        }
        Texture{id}
    }
    pub fn active(&self, number: u32){
        unsafe {
            gl::ActiveTexture(gl::TEXTURE0 + number);
        }
    }
}

impl Drop for Texture{
    fn drop(&mut self) {
        unsafe{
            gl::DeleteTextures(1, [self.id].as_ptr());
        }
    }
}

