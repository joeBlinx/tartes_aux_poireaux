#version 330 core

layout (location = 0) in vec3 Position;
layout(location =1) in vec2 pos_uv;
uniform float x;
uniform float y;
uniform float scale;
uniform float angle;
out vec2 uv;
void main()
{
    mat3 rotation = mat3(
     cos(angle), sin(angle), 0,
     -sin(angle), cos(angle), 0,
     0, 0, 1
    );
    mat3 translate = mat3(
    scale, 0, 0,
    0, scale, 0,
    x, y, 1
    );
    gl_Position = vec4(translate*rotation*Position, 1.0);
    uv = pos_uv  ;
}