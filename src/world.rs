use crate::entity::asteroid_factory::AsteroidFactory;
use crate::entity::bullet::Bullet;
use crate::entity::traits::*;
use crate::entity::{asteroid::Asteroid, triangle::*};
use crate::opengl::program::Program;
use crate::utils;

pub struct World {
    factory: AsteroidFactory,
    asteroids: Vec<Asteroid>,
    triangle: Triangle,
    bullets: Vec<Bullet>,
}

impl World {
    fn collide(&mut self) {
        let mut dead_bullet = Vec::new();
        let mut dead_ennemies = Vec::new();
        for i in 0..self.bullets.len() {
            for j in 0..self.asteroids.len() {
                let has_collision =
                    collision(self.bullets.get(i).unwrap(), self.asteroids.get(j).unwrap());
                if has_collision {
                    dead_bullet.push(i);
                    dead_ennemies.push(j);
                }
            }
        }
        for index in dead_bullet.iter().rev() {
            self.bullets.remove(*index);
        }
        for index in dead_ennemies.iter().rev() {
            self.asteroids.remove(*index);
        }
    }
    pub fn handle_input(&mut self, event: &sdl2::event::Event) {
        match event {
            sdl2::event::Event::KeyUp { keycode, .. } => {
                let keycode = keycode.unwrap();
                if keycode == sdl2::keyboard::Keycode::Space {
                    self.bullets.push(self.triangle.launch_bullet());
                }
            }
            _ => {}
        }
        self.triangle.handle_event(&event);
    }
    pub fn draw_all(&self, program: &mut Program) {
        for asteroid in &self.asteroids {
            asteroid.update_uniform(program);
            utils::draw();
        }
        self.triangle.draw(program);
        for bullet in &self.bullets {
            bullet.update_uniform(program);
            utils::draw();
        }
    }
}

fn collision(bullet: &Bullet, asteroid: &Asteroid) -> bool {
    let dx = bullet.base.x - asteroid.base.x;
    let dy = bullet.base.y - asteroid.base.y;
    let distance = (dx * dx + dy * dy).sqrt();

    return distance < bullet.base.scale + bullet.base.scale;
}

impl Default for World {
    fn default() -> Self {
        World {
            factory: AsteroidFactory::new(std::time::Duration::from_millis(500)),
            asteroids: Vec::default(),
            triangle: Triangle::default(),
            bullets: Vec::default(),
        }
    }
}
impl Update for World {
    fn update(&mut self) {
        for asteroid in &mut self.asteroids {
            asteroid.update();
        }
        self.triangle.update();
        for bullet in &mut self.bullets {
            bullet.update();
        }
        if let Some(asteroid) = self.factory.get_asteroid() {
            self.asteroids.push(asteroid);
        }
        let mut index_to_remove = Vec::new();
        for i in 0..self.asteroids.len() {
            let asteroid = self.asteroids.get(i).unwrap();
            if asteroid.base.y <= -1. {
                index_to_remove.push(i);
            }
        }
        for index in index_to_remove.iter().rev() {
            self.asteroids.remove(*index);
        }
        self.collide();
    }
}
