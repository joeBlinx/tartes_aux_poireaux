#version 330 core

out vec4 Color;
uniform sampler2D image;
in vec2 uv;
void main()
{
    Color = texture(image, uv);
}