use crate::opengl::program::Program;

pub trait Update {
    fn update(&mut self);
}
pub trait UpdateUniform {
    fn update_uniform(&self, program: &mut Program);
}
