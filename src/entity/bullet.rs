use crate::entity::entity::Entity;
use crate::entity::traits::{Update, UpdateUniform};
use crate::opengl::program::Program;

pub struct Bullet {
    pub base: Entity,
}
impl Bullet {
    pub fn new(x: f32, y: f32) -> Bullet {
        let base = Entity {
            x,
            y,
            scale: 0.025,
            color: [1., 0., 0.],
            ..Entity::default()
        };
        Bullet { base }
    }
}
impl Update for Bullet {
    fn update(&mut self) {
        self.base.move_xy(0., 2.);
    }
}
impl UpdateUniform for Bullet {
    fn update_uniform(&self, program: &mut Program) {
        self.base.update_uniform(program);
    }
}
