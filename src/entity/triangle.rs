use crate::entity::bullet::Bullet;
use crate::entity::entity::Entity;
use crate::entity::traits::{Update, UpdateUniform};
use crate::utils;
use sdl2;
use crate::opengl::program::Program;

enum Movement {
    None,
    Left,
    Right,
}

pub struct Triangle {
    base: Entity,
    need_to_move: Movement,
}
impl Triangle {
    pub fn move_xy(&mut self, x: f32, y: f32) {
        self.base.move_xy(x, y);
    }
    pub fn handle_event(&mut self, event: &sdl2::event::Event) {
        match event {
            sdl2::event::Event::KeyDown { keycode, .. } => {
                let keycode = keycode.unwrap();
                if keycode == sdl2::keyboard::Keycode::Left {
                    self.need_to_move = Movement::Left;
                } else if keycode == sdl2::keyboard::Keycode::Right {
                    self.need_to_move = Movement::Right;
                }
            }
            sdl2::event::Event::KeyUp { keycode, .. } => {
                let keycode = keycode.unwrap();
                if keycode == sdl2::keyboard::Keycode::Left
                    || keycode == sdl2::keyboard::Keycode::Right
                {
                    self.need_to_move = Movement::None;
                }
                if keycode == sdl2::keyboard::Keycode::Space {
                    self.launch_bullet();
                }
            }
            _ => {}
        }
    }

    pub fn launch_bullet(&mut self) -> Bullet {
        Bullet::new(self.base.x, self.base.y)
    }

    pub fn draw(&self, program: &mut Program) {
        self.update_uniform(program);
        utils::draw();
    }
}
impl Default for Triangle {
    fn default() -> Triangle {
        let base = Entity {
            color: [0., 1., 0.2],
            ..Entity::default()
        };
        Triangle {
            base,
            need_to_move: Movement::None,
        }
    }
}
impl Update for Triangle {
    fn update(&mut self) {
        let speed = 2.;
        match &self.need_to_move {
            Movement::Right => self.move_xy(speed, 0.),
            Movement::Left => self.move_xy(-speed, 0.),
            Movement::None => {}
        }
    }
}
impl UpdateUniform for Triangle {
    fn update_uniform(&self, program: &mut Program) {
        self.base.update_uniform(program);
    }
}
