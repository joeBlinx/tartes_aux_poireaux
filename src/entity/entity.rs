use crate::entity::traits::UpdateUniform;
use crate::opengl::program::Program;

pub struct Entity {
    pub x: f32,
    pub y: f32,
    pub speed: f32,
    pub scale: f32,
    pub angle: f32,
    pub color: [f32; 3],
}
impl Entity {
    pub fn move_xy(&mut self, x: f32, y: f32) {
        self.x += self.speed * x;
        self.y += self.speed * y;
    }
}
impl Default for Entity {
    fn default() -> Self {
        Entity {
            x: 0.,
            y: -0.9,
            speed: 0.01,
            scale: 0.05,
            angle: 0.,
            color: [0., 0., 0.],
        }
    }
}
impl UpdateUniform for Entity {
    fn update_uniform(&self, program: &mut Program) {
        program.set_uni("x", self.x);
        program.set_uni("y", self.y);
        program.set_uni("scale", self.scale);
        program.set_uni("angle", self.angle);
        program.set_uni("color", self.color);
    }
}
