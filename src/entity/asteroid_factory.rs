use crate::entity::asteroid::Asteroid;
use rand::Rng;
pub struct AsteroidFactory {
    last: std::time::Instant,
    delta: std::time::Duration,
}
use std::option::Option;

impl AsteroidFactory {
    pub fn new(delta: std::time::Duration) -> AsteroidFactory {
        AsteroidFactory {
            last: std::time::Instant::now(),
            delta,
        }
    }

    pub fn get_asteroid(&mut self) -> Option<Asteroid> {
        let now = std::time::Instant::now();
        let delta_time = now.duration_since(self.last);
        return if delta_time.as_millis() >= self.delta.as_millis() {
            self.last = now;
            let x: f32 = rand::thread_rng().gen_range(-0.9, 0.9);
            Some(Asteroid::new(x))
        } else {
            None
        };
    }
}
