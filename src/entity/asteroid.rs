use crate::entity::entity::Entity;
use crate::entity::traits::*;
use crate::opengl::program::Program;

pub struct Asteroid {
    pub base: Entity,
}
impl Asteroid {
    pub fn new(x: f32) -> Asteroid {
        Asteroid {
            base: Entity {
                x,
                y: 1.,
                angle: 3.14,
                color: [0., 0.5, 0.5],
                ..Entity::default()
            },
        }
    }
}

impl Update for Asteroid {
    fn update(&mut self) {
        self.base.move_xy(0., -0.5);
    }
}
impl UpdateUniform for Asteroid {
    fn update_uniform(&self, program: &mut Program) {
        self.base.update_uniform(program);
    }
}
