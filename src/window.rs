extern crate gl;
extern crate sdl2;

pub(crate) struct Window {
    sdl: sdl2::Sdl,
    _video_subsystem: sdl2::VideoSubsystem,
    window: sdl2::video::Window,
    _gl_context: sdl2::video::GLContext,
}

impl Window {
    pub(crate) fn new() -> Window {
        let sdl = sdl2::init().expect("Error while init sdl2");
        let video_subsystem = sdl.video().expect("Error while init sdl video");
        let gl_attr = video_subsystem.gl_attr();

        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(4, 5);
        let window = video_subsystem
            .window("Tartes aux poireaux", 800, 800)
            .opengl()
            .resizable()
            .build()
            .expect("Error while creating window");

        let gl_context = window.gl_create_context().expect("Error while init OpenGL");
        let _gl = gl::load_with(|s| {
            video_subsystem.gl_get_proc_address(s) as *const std::os::raw::c_void
        });
        unsafe {
            gl::ClearColor(0.5, 0.5, 0.5, 1.);
        }
        Window {
            sdl,
            _video_subsystem: video_subsystem,
            window,
            _gl_context: gl_context,
        }
    }

    pub(crate) fn sdl(&self) -> &sdl2::Sdl {
        &self.sdl
    }

    pub(crate) fn refresh(&self) {
        self.window.gl_swap_window();
    }
}
