mod opengl;
mod entity;
mod uniform;
mod utils;
mod window;
mod world;
use crate::entity::traits::Update;
use std::ffi::CString;
use crate::opengl::{shader, buffer, program};
use std::path::Path;
use crate::opengl::buffer::VboSettings;

extern crate gl;
extern crate rand;
extern crate sdl2;

fn main() {
    let window = window::Window::new();
    let sdl = window.sdl();
    let mut event = sdl.event_pump().unwrap();

    let vert_shader =
        shader::Shader::from_vert_source(&CString::new(include_str!("triangle.vert")).unwrap())
            .unwrap();

    let frag_shader =
        shader::Shader::from_frag_source(&CString::new(include_str!("triangle.frag")).unwrap())
            .unwrap();
    let mut shader_program = program::Program::from_shaders(&[vert_shader, frag_shader]).unwrap();


    let texture = opengl::texture::Texture::from_file(Path::new("test.png"));
    texture.active(0);
    shader_program.set_used();

    shader_program.set_uni("test", 1.);
    let vertices: Vec<f32> = vec![-1., -1., 1.0, 0., 1.,
                                  1., -1., 1.0, 1., 1.,
                                  0.0, 1., 1.0, 0.5, 0.];
    let vbo = buffer::Vbo::create(vertices);
    let vao = buffer::Vao::create(vbo, &[VboSettings{size:3, location:0, stride:5, offset:0},
        VboSettings{
            location:1,
            size:2,
            stride:5,
            offset:3
        }
    ]);
    vao.bind();
    let mut world = world::World::default();
    'main: loop {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
        }
        for event in event.poll_event() {
            world.handle_input(&event);
            match event {
                sdl2::event::Event::Quit { .. } => break 'main,
                _ => {}
            }
        }
        world.update();
        world.draw_all(&mut shader_program);
        let ten_millis = std::time::Duration::from_millis(17);
        std::thread::sleep(ten_millis);
        window.refresh();
    }
}
