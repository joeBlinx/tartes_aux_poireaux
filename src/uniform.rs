use gl::types::GLint;
pub trait SetUniform {
    fn set_uniform(&self, uni_id: GLint);
}

impl SetUniform for f32 {
    fn set_uniform(&self, uni_id: GLint) {
        unsafe {
            gl::Uniform1f(uni_id, *self);
        }
    }
}

impl SetUniform for [f32; 3] {
    fn set_uniform(&self, uni_id: GLint) {
        unsafe {
            gl::Uniform3f(
                uni_id,
                *self.get(0).unwrap(),
                *self.get(1).unwrap(),
                *self.get(2).unwrap(),
            );
        }
    }
}
